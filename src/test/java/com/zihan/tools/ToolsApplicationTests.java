package com.zihan.tools;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ToolsApplicationTests {

    @Test
    public void test(){
        int[] num = new int[3];
        num[0]=1;
        num[1]=3;
        num[2]=2;
        Arrays.sort(num);
        for (int i : num) {
            System.out.print(i+" ");
        }

    }

}
