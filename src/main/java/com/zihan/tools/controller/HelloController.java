package com.zihan.tools.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author chenzihan
 * @date 2019-08-08 15:52
 */
@Controller
public class HelloController {

    @RequestMapping(value = "/")
    public String hello(){
        return "tools/sqlformat/sql_format";
    }

    @RequestMapping(value = "win")
    public String win(){
        return "win";
    }

    @RequestMapping(value = "config")
    public String config(){
        return "config";
    }

    @RequestMapping(value = "layout")
    public String layout(){
        return "layout";
    }

    @RequestMapping(value = "nav")
    public String nav(){
        //修改2
        return "nav";
    }
}
