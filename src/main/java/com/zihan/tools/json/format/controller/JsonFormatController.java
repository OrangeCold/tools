package com.zihan.tools.json.format.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author chenzihan
 * @date 2019-08-09 16:07
 */
@Controller
public class JsonFormatController {

    /**
     * 页面跳转
     */
    @RequestMapping(value = "/json/format")
    public String toPage(){
        return "tools/jsonformat/json_format";
    }

    @ResponseBody
    @PostMapping(value = "/json/turn")
    public String turn(@RequestParam("text")String text){
        return text;
    }

}
