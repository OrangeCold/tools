package com.zihan.tools.sql.format.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author chenzihan
 * @date 2019-08-08 15:59
 */
@Controller
public class SqlFormatController {

    /**
     * 跳转到页面
     */
    @RequestMapping(value = "/sqlformat/sql")
    public String toPage(){
        return "tools/sqlformat/sql_format";
    }

    /**
     * 格式化，去除连接符 ”+“
     * @param sql
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/sqlformat/conn")
    public String conn(@RequestParam("sql")String sql){

        sql = sql.replace("\"","");
        sql = sql.replace("+","");

        return sql;
    }
}
