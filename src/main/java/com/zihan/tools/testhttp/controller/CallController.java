package com.zihan.tools.testhttp.controller;

import com.zihan.util.HttpUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author chenzihan
 * @date 2019-10-21 14:24
 */
@Controller
public class CallController {

    // 回调接口的url
//    String url = "http://10.2.6.103:8092/edwfms/setResults";
    String url = "http://10.2.153.51:8086/edwfms/setResults";

    @RequestMapping(value = "sendEngData",method = RequestMethod.POST)
    public String sendEngData(@RequestBody String transferDataJson) {
        JSONArray jsonArray = JSONArray.fromObject(transferDataJson);
        for (Object o : jsonArray) {
            JSONObject jsonObject = JSONObject.fromObject(o);
            int id = jsonObject.getInt("ID");
            System.out.println("id = " + id);
            String results = "true";
            JSONObject json = new JSONObject();
            json.element("ID",id);
            json.element("CALL_RESULTS",results);
            String s = json.toString();
            String s1 = HttpUtil.sendPost(url, s);
            System.out.println("s1 = " + s1);
        }
        return null;
    }

    @ResponseBody
    @RequestMapping("test")
    public String test(){
        String sendSmsUrl = "http://10.2.153.51:8085/mbes/sendSms";
        String updatePhoneUrl = "http://localhost:8086/tmp/mobile/updatePhone";
        String getCarUrl = "http://localhost:8080/vmp/wechat/getKeyCipher";


        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.element("apply_code","BH19061016400130");
        String voResult = HttpUtil.sendPost(getCarUrl, jsonObject1.toString());
        System.out.println("voResult = " + voResult);


        return "success";
    }

}
