package com.zihan.util;

import java.util.Date;

/**
 * @author chenzihan
 * @date 2019-11-08 13:57
 */
public class Test {

    public static void main(String[] args) {
        Date appointmentDate = new Date();
        System.out.println("appointmentDate = " + appointmentDate);
        long time = 2*60*1000;
        Date appoint = new Date(appointmentDate.getTime()-time);
        System.out.println("appoint = " + appoint);

    }

}
