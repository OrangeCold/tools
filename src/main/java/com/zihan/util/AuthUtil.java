package com.zihan.util;

public class AuthUtil {
	/***
	 * 简单的签名,通过私钥+时间戳
	 * @Title: getSignature 
	 * @Description: TODO
	 * @param 
	 * @return String
	 * @throws 
	 * @author  zengshaobing
	 * @date 2017年4月18日 下午1:12:33
	 */
	public static String getSignature(String secretkey, long timestamp){
		if(secretkey==null || "".equals(secretkey.trim())){
			return null;
		}else {
			return MD5.md5(secretkey + timestamp);
		}
	}
	
	/***
	 * 获取需要验证的请求参数串 如:token=test&timestamp=1492489602996&signature=3c0e52e0b50fac39ecbd394c6764b769
	 * @Title: getRequestAuthParams 
	 * @Description: TODO
	 * @param 
	 * @return String
	 * @throws 
	 * @author  zengshaobing
	 * @date 2017年4月18日 下午1:16:11
	 */
	public static String getRequestAuthParams(String token, String secretkey){
		if(token==null || "".equals(token.trim()) || secretkey==null || "".equals(secretkey.trim()) ){
			System.err.println("参数为空");
			return null;
		}
		StringBuilder sb = new StringBuilder();
		long time = System.currentTimeMillis();
		sb.append("token=").append(token).append("&timestamp=").append(time).append("&signature=").append(getSignature(secretkey,time));
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		System.out.println(getRequestAuthParams("tokentest","QSH_TEST_SECRETKEY"));
	}
}
